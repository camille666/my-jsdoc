/**
 * $()函数
 * @returns {array}} 数组
 */
function $(){
  var elements = [];
  for(var i = 0, len = arguments.length; i < len; i++){
    var element = els[i];
    if(typeof element === 'string'){
        element = document.getElementById(element);
    }
    elements.push(element);
  }
  return elements;
}

/**
 * $()函数，工厂函数
 */

(function(){
  function _$(els){
    this.element = [];
    for(var i = 0, len = els.length; i < len; i++){
      var element = els[i];
      if(typeof element === 'string'){
        element = document.getElementById(element);
      }
      this.element.push(element);
    }
    return this;
  }
  window.$ = function(){
    return new _$(arguments);
  }
})();

/**
 * 获取元素
 * @param {string} name 类型
 * @param {string} val 值
 * @returns {object} sele 元素
 */

function getElementBy(name, val){
  var sele;
  if(name == 'id'){
    sele = document.getElementById(val);
  }else if(name == 'qsa'){
    sele = document.querySelectorAll("."+val);
  }else{
    alert('选择器错误！');
  }
  return sele;
}

/**
 * 获取类名
 * @param  {string} oParent   父类id选择器名称
 * @param  {string} className 子类class选择器名称
 * @returns {array} arr 数组
 */
function getClassName(oParent, className) 
{ 
  var oParent = oParent ? document.getElementById(oParent) : document;
  //判断浏览器是否支持getElementsByClassName，如果支持就直接用。
  if (document.getElementsByClassName){ 
    return oParent.getElementsByClassName(className);
  }else{ 
    var aEle = oParent.getElementsByTagName('*');   //获取指定元素
    var arr = [];  //这个数组用于存储所有符合条件的元素
    for(var i = 0; i < aEle.length; i++){ //遍历获得的元素
      if (aEle[i].className == className){ 
        //如果获得的元素中的class的值等于指定的类名，就赋值给tagnameAll
        arr[arr.length] = aEle[i];
        //方法2：arr.push(aEle[i]);
      }
     //方法3
     //if(aEle[i].className.indexOf(className) != -1){
     //  arr[arr.length] = aEle[i];
     //}  
    }
    return arr;
  }
}

/**
 * 判断是否有class
 * @param {object} elem 元素
 * @param {string} cls 类名
 * @returns {boolean} 是否有class
 */
function hasClass(elem, cls) {
  cls = cls || '';
  if (cls.replace(/\s/g, '').length == 0) return false; //当cls没有参数时，返回false
  return new RegExp(' ' + cls + ' ').test(' ' + elem.className + ' ');
}

/**
 * 添加class
 * @param {object} elem 元素
 * @param {string} cls 类名
 */
function addClass(elem, cls) {
  if (!hasClass(elem, cls)) {
    elem.className = elem.className == '' ? cls : elem.className + ' ' + cls;
  }
}

/**
 * 删除class
 * @param {object} elem 元素
 * @param {string} cls 类名
 */
function removeClass(elem, cls) {
  if (hasClass(elem, cls)) {
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, '') + ' ';
    while (newClass.indexOf(' ' + cls + ' ') >= 0) {
      newClass = newClass.replace(' ' + cls + ' ', ' ');
    }
    elem.className = newClass.replace(/^\s+|\s+$/g, '');
  }
}

/**
 * 获取属性值
 * @param {object} elem 元素
 * @param {string} name 属性名称
 */
function getAttr(elem, name){
  return elem.getAttribute(name);
}

/**
 * 显示隐藏
 * @param {object} elem 元素
 * @param {string} type 类型
 */
function showHide(elem,type){
  if(type == 'show'){
    elem.style.display = 'block';
  }else if(type == 'hide'){
    elem.style.display = 'none';
  }
}

/**
 * 设置标签内容
 * @param {object} elem 元素
 * @param {string} val 值
 */
function setHtml(elem,val){
  elem.innerHTML = val;
}


/**
 * 追加元素
 * @param {object} elem 元素
 * @param {string} label 标签
 * @param {string} val 值
 */
function apdElem(elem,label,val){
  var nelem = document.createElement(label),
  nelemText = document.createTextNode(val);
  nelem.appendChild(nelemText);   
  elem.appendChild(nelem);   
}

/**
 * 创建一个片段，常用于列表页
 * 您可能需要动态地追加多个元素到文档中，如果直接将它们插入到文档中会导致这个文档每次都需要重新布局一个，而使用文档碎片，建成后只需追加一次。
 */
(function createList() {
  var oLiArr = ["first li", "second li", "third li", "fourth li", "fifth li"];
  var oFrag = document.createDocumentFragment();
  while (oLiArr.length) {
    var nLi = document.createElement("li");　　　
    nLi.appendChild(document.createTextNode(oLiArr.shift()));
    oFrag.appendChild(nLi);
  }
  document.getElementById('J_ul').appendChild(oFrag);
})();

/**
 * 绑定事件
 * @param {object} elem 元素
 * @param {string} type 函数类型名称
 * @param {function} handler 函数
 */
function addEvent(elem,type,handler){
  if(elem.addEventListener){
    elem.addEventListener(type,handler,false);//默认是冒泡
  }else if(elem.attachEvent){
    elem.attachEvent('on'+type,handler);
  }else{
    elem['on'+type] = handler;
  }
}

/**
 * 从地址栏获取参数
 * @param {string} name 字符串名称
 * @returns {string} 字符串
 */
function getQueryString(name){
  var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if(r!=null)
  return encodeURIComponent(r[2]); 
  return null;
}

/**
 * 把URL的查询参数解析成字典对象
 * @param {string} url 地址
 * @returns {object} 对象
 */
function getQueryObject(url) {
  url = url == null ? window.location.href : url;
  var search = url.substring(url.lastIndexOf("?") + 1);
  var obj = {};
  var reg = /([^?&=]+)=([^?&=]*)/g;
  search.replace(reg, function (rs, $1, $2) {
      var name = decodeURIComponent($1);
      var val = decodeURIComponent($2);                
      val = String(val);
      obj[name] = val;
      return rs;
  });
  return obj;
}


/**
 * getXHR 创建XMLHttpRequest对象实例
 * @returns {object} xhr对象
 */
function getXHR(){
  var xhr = null;
  if(window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) { 
        alert("您的浏览器暂不支持Ajax!");
      }
    }
  }
  return xhr;
}

/**
 * getORpost 封装get和post方法
 * @param {string} url 地址
 * @param {object} data 数据
 * @param {string} method 方法
 * @param {boolean} async 是否异步
 * @param {function} callback 回调
 */
function getORpost(url, data, method, async, callback) {
  //默认异步
  if (async === undefined) {
    async = true;
  }
  var xhr = getXHR();
  //初始化HTTP请求
  xhr.open(method, url, async);
  xhr.onreadystatechange = function() {
    //readyState 的值等于4，从服务器拿到了数据
    if (xhr.readyState == 4) {
      //回调服务器响应数据
      callback(xhr.responseText);
    }
  };
  if (method == 'POST') {
    //给指定的HTTP请求头赋值
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  }
  //发送HTTP请求
  xhr.send(data);
};

/**
 * getFn get方法
 * @param {string} url 地址
 * @param {object} data 数据
 * @param {boolean} async 是否异步
 * @param {function} callback 回调
 */
function getFn(url, data, async, callback){
  var query = [];
  for (var key in data) {
    query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
  }
  getORpost(url + (query.length ? '?' + query.join('&') : ''), null, 'GET', async, callback);
}

/**
 * postFn post方法
 * @param {string} url 地址
 * @param {object} data 数据
 * @param {boolean} async 是否异步
 * @param {function} callback 回调
 */
function postFn(url, data, async, callback){
  var query = [];
  for (var key in data) {
    query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
  }
  getORpost(url, query.join('&'), 'POST', async, callback);
}

/**
 * 对外开放的接口参数及回调函数
 * @param {string} url 地址
 * @param {object} data 数据
 * @param {boolean} async 是否异步
 * @param {function} callback 回调
 */
var myObj = {
  "type": "get",
  "url": "/site.com",
  "data": {},
  complete: function(){

  },
  success: function(){

  },
  error: function(){

  }
}
// 1.获得ajax
if (window.XMLHttpRequest) { //查看当前浏览器XMLHttpRequest是否是全局变量
  var oAjax = new XMLHttpResquest();
} else {
  var oAjax = new ActiveXObject('Microsoft.XMLHTTP'); //IE6,传入微软参数
}

// 2.打开地址
switch (myObj.type.toLowerCase()) {
  case 'get':
    oAjax.open('GET', myObj.url + '?' + jsonToURL(myObj.data), true); // 提交方式(大写)，url，是否异步
    oAjax.send(); // 3.发送数据
  break;
  case 'post':
    oAjax.open('POST', myObj.url, true);
    oAjax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    oAjax.send(jsonToURL(myObj.data)); // 3.发送数据
  break;
}

// 4.接收数据
oAjax.onreadystatechange = function() { //监控状态
  if (oAjax.readyState == 4) {
    myObj.complete && myObj.complete();
    if (oAjax.status >= 200 && oAjax.status < 300 || oAjax.status == 304) {
      myObj.success && myObj.success(oAjax.responseText); //执行成功的回调函数, responseText为响应内容
    } else {
      myObj.error && myObj.error(oAjax.status); //执行失败的回调函数
    }
  }
};

/**
 * 求字符串的字节长度，for循环实现
 * @param {string} str 字符串
 * @returns {number} 长度
 */
var strByteLen = function(str){
  var byteLen=0,len=str.length;
  if(str){
    for(var i=0; i<len; i++){
      if( str.charCodeAt(i) > 255 ){
        byteLen += 2;
      }else{
        byteLen++;
      }
    }
    return byteLen;
  }else{
    return 0;
  }
}

/**
 * 求字符串的字节长度，正则表达式实现
 * @param {string} str 字符串
 * @returns {number} 长度
 */
var strByteLen = function(str){
　return str.replace(/[^x00-xFF]/g,'**').length;
};

/**
 * html实体替换函数
 * @param {string} str 字符串
 * @returns {string} str
 */
function escapeHTML(str) {  
  var replacements= {"<": "&lt;", ">": "&gt;","&": "&amp;", "\"": "&quot;"};                      
  return str.replace(/[<>&"]/g, function(character) {  
    return replacements[character];  
  }); 
}

 /**
 * 移动端touch事件，上下左右滑
 */
var startX, startY, moveEndX, moveEndY;
document.body.ontouchstart = function(e) {
  e.preventDefault();
  startX = e.touches[0].pageX,
  startY = e.touches[0].pageY;
};
document.body.ontouchmove = function(e) {
  e.preventDefault();
  moveEndX = e.touches[0].pageX,
  moveEndY = e.touches[0].pageY,
  X = moveEndX - startX,
  Y = moveEndY - startY;
  if (Math.abs(X) > Math.abs(Y) && X > 0) {//从左往右滑
    doSomethingOne();
  }else if (Math.abs(X) > Math.abs(Y) && X < 0) {//从右往左滑
    doSomethingTwo();
  }else if(Math.abs(Y) > Math.abs(X) && Y > 0) {//从上往下滑
    doSomethingThree();
  }else if(Math.abs(Y) > Math.abs(X) && Y < 0) {//从下往上滑
    doOtherThingFour();
  }
};

/**
 * 函数柯里化封装
 * @param {function} func 函数
 * @param {number} arity 用来标记剩余参数的个数
 * @param {array} args 用来收集参数
 * @returns {function} 函数 
 */
function createCurry(func, arity, args) {
  // 第一次执行时，并不会传入arity，而是直接获取func参数的个数 func.length
  var arity = arity || func.length;
  // 第一次执行也不会传入args，而是默认为空数组
  var args = args || [];
  var wrapper = function() {
    // 将wrapper中的参数收集到args中
    var _args = [].slice.call(arguments);
    [].push.apply(args, _args);

    // 如果参数个数小于最初的func.length，则递归调用，继续收集参数
    if (_args.length < arity) {
      arity -= _args.length;
      return createCurry(func, arity, args);
    }
    // 参数收集完毕，则执行func
    return func.apply(func, args);
  }
  return wrapper;
}

/**
 * 获取当前浏览器支持的transform兼容写法
 * 如果返回的为空字符串，则表示当前浏览器并不支持transform，
 * 这个时候我们就需要使用left，top值来改变元素的位置。如果支持，就改变transform的值。
 * @returns {string} 字符串 
 */
function getTransform() {
  var transform = '',
      divStyle = document.createElement('div').style,
      // 可能涉及到的几种兼容性写法，通过循环找出浏览器识别的那一个
      transformArr = ['transform', 'webkitTransform', 'MozTransform', 'msTransform', 'OTransform'],

      i = 0,
      len = transformArr.length;

  for(; i < len; i++)  {
    if(transformArr[i] in divStyle) {
      // 找到之后立即返回，结束函数
      return transform = transformArr[i];
    }
  }
  // 如果没有找到，就直接返回空字符串
  return transform;
}

/**
 * 获取元素样式
 * @param {object} elem 元素
 * @param {string} property 属性名称
 * @returns {string} 字符串 
 */
function getStyle(elem, property) {
  // ie通过currentStyle来获取元素的样式，其他浏览器通过getComputedStyle来获取
  return document.defaultView.getComputedStyle ? document.defaultView.getComputedStyle(elem, false)[property] : elem.currentStyle[property];
}