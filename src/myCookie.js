/**
 * 获取cookie
 * @param {string} cname cookie名称
 * @returns {string} cookieValue cookie值
 */
function getCookie(cname){
  if(document.cookie.length > 0){
    var cookieName = encodeURIComponent(cname) + "=",
    cookieStart = document.cookie.indexOf(cookieName),
    cookieValue = null;
    if(cookieStart > -1){
      cookieStart = cookieStart + cname.length + 1;
      var cookieEnd = document.cookie.indexOf(";",cookieStart);
      if(cookieEnd == -1){
        cookieEnd = document.cookie.length;
      }
      cookieValue = decodeURIComponent(document.cookie.substring(cookieStart,cookieEnd));
    }
    return cookieValue;
  }else{
    return "";
  }
}

/**
 * 设置cookie
 * @param {string} cname ，必要，名字；
 * @param {string} cval ，必要，值；
 * @param {boolean} cexdate，可选，永不过期的cookie为Infinity，cookie失效的时间，最大年龄秒数；
 * 如果为正数，该cookie在maxAge秒之后失效。
   如果为负数，该cookie为临时cookie，关闭浏览器即失效，浏览器也不会以任何形式保存该cookie。
   如果为0，表示删除该cookie，默认为-1。
 * @param {string} cpath，可选，默认为当前文档位置的路径，最后一个字符必须为“/”。
 * @param {string} cdomain，可选，默认为当前文档位置的路径的域名部分，第一个字符必须为“.”。
 * @param {string} csecure，可选，cookie只会被https传输。
 * @returns {boolean} 
 */
function setCookie(cname, cval, cexdate, cpath, cdomain, csecure){
  if (!cname || /^(?:expires|max\-age|path|domain|secure)$/i.test(cname)) { 
    return false; 
  }
  var cExpires = "";
  if (cexdate) {
    switch (cexdate.constructor) {
      case Number:
        cExpires = cexdate === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + cexdate;
        break;
      case String:
        cExpires = "; expires=" + cexdate;
        break;
      case Date:
        cExpires = "; expires=" + cexdate.toGMTString();
        break;
    }
  }
  document.cookie = encodeURIComponent(cname) + "=" + encodeURIComponent(cval) + cExpires + (cdomain ? "; domain=" + cdomain : "") + (cpath ? "; path=" + cpath : "") + (csecure ? "; secure" : "");
  return true;
}

/**
 * 测试
 */
setCookie("test1", "Unicode test: \u00E0\u00E8\u00EC\u00F2\u00F9", Infinity);
setCookie("test2", "Hello world!", new Date(2020, 5, 12));
setCookie("test3", "Hello world!", new Date(2027, 2, 3), "/blog");
setCookie("test4", "Hello world!", "Sun, 06 Nov 2022 21:43:15 GMT");
setCookie("test5", "Hello world!", "Tue, 06 Dec 2022 13:11:07 GMT", "/home");

/**
 * 是否有cookie
 * @param {string} cname 名称
 * @returns {string} cookies cookies
 */
function hasCookie(cname){
  var self = this,
  cookies = decodeURIComponent(self.getCookie(cname));
  return !!cookies;
}

/**
 * 删除cookie方案1
 * 新建同名cookie，设置过期时间，覆盖旧cookie；
 * @param {string} cname 名称
 * @param {string} cpath 路径
 * @param {string} cdomain 域名 
 * @returns {boolean} boolean 布尔
 */
function deleteCookie1(cname, cpath, cdomain){
  if (!cname || !this.hasCookie(cname)) { 
    return false; 
  }
  document.cookie = encodeURIComponent(cname) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + ( cdomain ? "; domain=" + cdomain : "") + ( cpath ? "; path=" + cpath : "");
  return true;
}

/**
 * 删除cookie方案2
 * 将setCookie()函数中的第二个参数设置为空值；
 * 第三个参数设置为小于系统的当前时间；
 * @param {string} cname 名称
 * @param {string} cpath 路径
 * @param {string} cdomain 域名 
 * @returns {boolean} boolean 布尔
 */
function deleteCookie2(cname, cpath, cdomain){
  if (!cname || !this.hasCookie(cname)) { 
    return false; 
  }
  setCookie(cname,'' -1);
  return true;
}

