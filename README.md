## jsdoc

一个JavaScript API文档生成器

1、生成文件

````
yarn jsdoc:generate
````

2、预览文件

````
yarn jsdoc:dev
````

3、问题答疑

如果修改了配置文件，比如jsdoc.json，就需要重新生成docs，再起服务。